# -*- Makefile -*-

AR         ?= ar
ARFLAGS    ?= cr
RANLIB     ?= ranlib
RM         ?= rm
RM_RF      ?= $(RM) -rf
RMDIR      ?= rmdir
