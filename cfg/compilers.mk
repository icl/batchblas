# -*- Makefile -*-

NVCC ?= nvcc
NVCFLAGS ?= -Wno-deprecated-gpu-targets

CC ?= $(NVCC)
CFLAGS ?= $(CINCLUDE) $(NVCFLAGS)
