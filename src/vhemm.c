#include <cblas.h>

#include <batchblas.h>

void blas_vhemm_batch(int group_count, const int *group_sizes,
		      bblas_layout_t layout, const bblas_side_t *side, const bblas_uplo_t *uplo,
		      const int *m, const int *n, 
		      const bblas_complex16_t *alpha, bblas_complex16_t const *const *A, const int *lda, 
		      				      bblas_complex16_t const* const *B, const int *ldb, 
		      const bblas_complex16_t *beta,  bblas_complex16_t** C, const int *ldc, 
		      int *info) {

  int ig, iter, prefix_sum;

  prefix_sum = 0;
  for (ig = 0; ig < group_count; ++ig) {
    for (iter = 0; iter < group_sizes[ig]; ++iter) {
		cblas_vhemm(layout, side[ig], uplo[ig],
				m[ig], n[ig],
				alpha[ig], A[prefix_sum + iter], lda[ig],
				B[prefix_sum + iter], ldb[ig],
				beta[ig],  C[prefix_sum + iter], ldc[ig]);
    }
    prefix_sum += group_sizes[ig];
  }

  return 0;
}
