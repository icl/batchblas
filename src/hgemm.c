#ifdef BATCHBLAS_HAVE_CBLAS
#include <cblas.h>
#else
#include <batchblas_cblas.h>
#endif

#include <batchblas.h>

int
BatchBlas_hgemm(batchblas_layout_t layout,
  int group_count, const int *group_sizes,
  const batchblas_transop_t *transA, const batchblas_transop_t *transB,
  int *m, int *n, int *k,
  batchblas_float16_t *alpha,
  batchblas_float16_t **A, int *ldA,
  batchblas_float16_t **B, int *ldB,
  batchblas_float16_t *beta,
  batchblas_float16_t **C, int *ldC,
  int *info) {

  int ig, iter, prefix_sum;

  prefix_sum = 0;
  for (ig = 0; ig < group_count; ++ig) {
    for (iter = 0; iter < group_sizes[ig]; ++iter) {
      cblas_hgemm((CBLAS_LAYOUT)layout,
        (CBLAS_TRANSPOSE)transA[prefix_sum + iter], (CBLAS_TRANSPOSE)transB[prefix_sum + iter],
        m[prefix_sum + iter], n[prefix_sum + iter], k[prefix_sum + iter],
        alpha+prefix_sum + iter,
        A[prefix_sum + iter], ldA[prefix_sum + iter],
        B[prefix_sum + iter], ldB[prefix_sum + iter],
        beta+prefix_sum + iter,
        C[prefix_sum + iter], ldC[prefix_sum + iter]);
    }
    prefix_sum += group_sizes[ig];
  }

  return 0;
}
