#include <cblas.h>

#include <batchblas.h>

void blas_vherk_batch(int group_count, const int *group_sizes,
		      bblas_layout_t layout, const bblas_uplo_t *uplo, const bblas_trans_t *trans,
		      const int *n, const int *k, 
		      const double *alpha, bblas_complex16_t const *const *A, const int *lda, 
		      const double  *beta, bblas_complex16_t		** C, const int *ldc, 
    		      int *info) {

  int ig, iter, prefix_sum;

  prefix_sum = 0;
  for (ig = 0; ig < group_count; ++ig) {
	  for (iter = 0; iter < group_sizes[ig]; ++iter) {
		  cblas_vherk(layout, uplo[ig], trans[ig],
                    		n[ig], k[ig],
                    		alpha[ig], A[prefix_sum + iter], lda[ig],
                    		beta[ig],  C[prefix_sum + iter], ldc[ig]);
    }
    prefix_sum += group_sizes[ig];
  }

  return 0;
}
