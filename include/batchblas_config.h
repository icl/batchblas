#ifndef BATCHBLAS_CONFIG_H
#define BATCHBLAS_CONFIG_H 1

#ifdef __CUDACC__
#include <cuda.h>
#include <cuda_fp16.h>
typedef __half _Float16;
typedef __half2 BatchBlas_Complex_Float16;
#elif defined(__GNUC__) && defined(__ARM_NEON__) && defined(__ARM_NEON_FP) && defined(__ARM_FEATURE_SIMD32)
typedef __fp16 _Float16;
typedef _Complex __fp16  BatchBlas_Complex_Float16;
#elif defined(__APPLE__) && defined(__APPLE_CC__) && (__APPLE_CC__ >= 6000)
/* This is Apple's LLVM that has _Float16 but doesn't allow _Complex _Float16 */
typedef _Float16 BatchBlas_Complex_Float16[2];
#if defined(__F16C__) && defined(__FLT16_MIN__)
#endif
#else
/* this is fail-safe fall-back */
#include <stdint.h>
typedef uint16_t _Float16;
typedef uint32_t BatchBlas_Complex_Float16;
#endif

#endif
