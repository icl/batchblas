#ifndef BATCHBLAS_ENUM_H
#define BATCHBLAS_ENUM_H 1

typedef enum bblas_layout_e {
  bblas_rowmajor = 101
, bblas_colmajor = 102
} bblas_layout_t;

typedef enum bblas_transop_e {
  bblas_notrans   = 111
, bblas_trans     = 112
, bblas_conjtrans = 113
} bblas_trans_t;

typedef enum bblas_uplo_e {
  bblas_upper = 121
, bblas_lower = 122
} bblas_uplo_t;

typedef enum bblas_diag_e {
  bblas_diag_nonunit = 131
, bblas_diag_unit    = 132
} bblas_diag_t;

typedef enum bblas_side_e {
  bblas_side_left  = 141
, bblas_side_light = 142
} bblas_side_t;

enum bblas_fp_property_e {
  bblas_base      = 151
, bblas_mantissa  = 152
, bblas_rnd       = 153
, bblas_ieee      = 154
, bblas_emin      = 155
, bblas_emax      = 156
, bblas_eps       = 157
, bblas_prec      = 158
, bblas_underflow = 159
, bblas_overflow  = 160
, bblas_sfmin     = 161
} bblas_fp_property_t;

enum blas_prec_e {
  blas_prec_single     = 211
, blas_prec_double     = 212
, blas_prec_indigenous = 213
, blas_prec_extra      = 214
, blas_prec_extended   = 215
, blas_prec_quad       = 216
, blas_prec_half       = 217
, blas_prec_quarter    = 218
, blas_prec_bfloat     = 219
};

#endif
