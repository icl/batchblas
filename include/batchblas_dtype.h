#ifndef BATCHBLAS_DTYPE_H
#define BATCHBLAS_DTYPE_H 1

typedef _Float16 bblas_float16_t;
typedef float  bblas_float32_t;
typedef double bblas_float64_t;
typedef BatchBlas_Complex_Float16 bblas_complex16_t;
typedef _Complex float  bblas_complex32_t;
typedef _Complex double bblas_complex64_t;

#endif
