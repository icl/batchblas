#ifndef BATCHBLAS_CBLAS_H
#define BATCHBLAS_CBLAS_H 1

typedef enum {CblasRowMajor=101, CblasColMajor=102} CBLAS_LAYOUT;
typedef enum {CblasNoTrans=111, CblasTrans=112, CblasConjTrans=113} CBLAS_TRANSPOSE;
typedef enum {CblasUpper=121, CblasLower=122} CBLAS_UPLO;
typedef enum {CblasNonUnit=131, CblasUnit=132} CBLAS_DIAG;
typedef enum {CblasLeft=141, CblasRight=142} CBLAS_SIDE;

typedef CBLAS_LAYOUT CBLAS_ORDER; /* this is for backward compatibility with CBLAS_ORDER */

void
cblas_hgemm(CBLAS_LAYOUT layout, CBLAS_TRANSPOSE transA, CBLAS_TRANSPOSE transB,
  const int M, const int N, const int K,
  const void *alpha,
  const void *A, const int lda, const void *B, const int ldb,
  const void *beta, void *C, const int ldc);

void
cblas_vgemm(CBLAS_LAYOUT layout, CBLAS_TRANSPOSE transA, CBLAS_TRANSPOSE transB,
  const int M, const int N, const int K,
  const void *alpha,
  const void *A, const int lda, const void *B, const int ldb,
  const void *beta, void *C, const int ldc);

#endif
